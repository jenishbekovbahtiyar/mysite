from django.template.context_processors import static
from django.conf.urls import url, patterns
from django.contrib import admin
from mysite import views
from mysite.views import GlavaView

urlpatterns = {
    url(r'^Glava/', GlavaView.as_view()),
    url(r'^admin/', admin.site.urls),
}